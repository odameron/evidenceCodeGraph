#! /usr/bin/env python3

import graphviz
import rdflib


g=rdflib.Graph()
g.load("./data/eco.owl")

query = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

PREFIX eco: <http://purl.obolibrary.org/obo/ECO_>

#SELECT DISTINCT ?ec1 ?ecLabel1 ?ec2 ?ecLabel2
SELECT DISTINCT ?ecLabel1 ?ecLabel2

WHERE {
  ?ec1 eco:9000002 ?ecLabel1 . # has GO evidence code
  FILTER NOT EXISTS {
    ?ec1 owl:deprecated "true"^^xsd:boolean .
  }
  ?ec1 rdfs:subClassOf ?ec2 .
  FILTER NOT EXISTS {
    ?ec2 owl:deprecated "true"^^xsd:boolean .
  }
  ?ec2 eco:9000002 ?ecLabel2 . # has GO evidence code
  FILTER (?ecLabel1 != ?ecLabel2)
}
"""

dotGraph = graphviz.Digraph('evidenceCode') 
dotGraph.attr(rankdir='BT')

qres = g.query(query)
for row in qres:
    print(f"{row.ecLabel1} -> {row.ecLabel2}")
    dotGraph.edge(row.ecLabel1, row.ecLabel2, arrowhead='empty')

dotGraph.render(filename='eco.dot')
