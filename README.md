# evidenceCodeGraph

Script for generating a graph of the subsumption relations between evidence codes


## Dependencies

- [Python](https://www.python.org/)
- [graphviz library for Python](https://graphviz.readthedocs.io/en/stable/)
- [RDFLib for Python](https://rdflib.readthedocs.io/en/stable/)


## Usage


### Download the Evidence and Conclusion ontology ECO

The following command will create a `data` subdirectory if it does not exist, and retrieve the latest stable version of the [Evidence and Conclusion ontology ECO](https://www.evidenceontology.org/).
Note that if the `data` subdirectory already contains an `eco.owl` file, it will be overwritten.

```bash
python3 downloadOntology
```


### Generate the graph based on evidence codes


```bash
python3 eco2graph.py
```

![Result of eco2graph.py](./eco.png)*Result of eco2graph.py*



### Generate the graph based on hierarchy of classes


```bash
python3 eco2classHierarchy.py
```

![Result of eco2classHierarchy.py](./eco_hierarchy.png)*Result of eco2classHierarchy.py colored according to the evidence codes*


## Todo

- [ ] path to directory and file name for downloading the ECO ontology should be parameters
- [ ] investigate the circular `rdfs:subClassOf` relations between IEA and EXP
