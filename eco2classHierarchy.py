#! /usr/bin/env python3

import graphviz
import rdflib


g=rdflib.Graph()
g.load("./data/eco.owl")

query = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

PREFIX eco: <http://purl.obolibrary.org/obo/ECO_>


SELECT DISTINCT ?ec ?ecLabel ?ecCode ?superEC ?superECLabel ?superECCode
WHERE {
  ?ec rdfs:subClassOf+ eco:0000000
  FILTER NOT EXISTS {
    ?ec owl:deprecated "true"^^xsd:boolean .
  }
  OPTIONAL {
    ?ec rdfs:label ?ecLabel .
  }
  OPTIONAL {
    ?ec eco:9000002 ?ecCode . # has GO evidence code
  }

  ?ec rdfs:subClassOf ?superEC .
  FILTER NOT EXISTS {
    ?superEC owl:deprecated "true"^^xsd:boolean .
  }
  OPTIONAL {
    ?superEC rdfs:label ?superECLabel .
  }
  OPTIONAL {
    ?superEC eco:9000002 ?superECCode . # has GO evidence code
  }
}
"""

dotGraph = graphviz.Digraph('evidenceCode') 
dotGraph.attr(rankdir='BT')

qres = g.query(query)
for row in qres:
    print(f"{row.ecLabel} ({row.ecCode}) -> {row.superECLabel} ({row.superECCode})")
    ecIdent = row.ec.replace("http://purl.obolibrary.org/obo/", "")
    superECIdent = row.superEC.replace("http://purl.obolibrary.org/obo/", "")
    dotGraph.node(ecIdent, label=row.ecLabel, evidenceCode=row.ecCode)
    dotGraph.node(superECIdent, label=row.superECLabel, evidenceCode=row.superECCode)
    dotGraph.edge(ecIdent, superECIdent, arrowhead='empty')

dotGraph.render(filename='eco_hierarchy.dot')
